import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
declare var $:any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    public router: Router,
    public auth: AngularFireAuth,
  ) { }

  ngOnInit() {
    document.body.className = 'hold-transition login-page';
    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
 
  }
  
  data:any={};
  login() {
    this.auth.signInWithEmailAndPassword(this.data.email, this.data.password).then(res => {
      this.router.navigate(['dashboard/home']);
    }).catch(error => {
      alert('Email atau password salah');
    })
  }
  }

