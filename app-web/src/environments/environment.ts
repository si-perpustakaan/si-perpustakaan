// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
 
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebase : {
    apiKey: "AIzaSyBWQAOf3Tpz4afl_ZxdkKT0mmAehSttdmw",
    authDomain: "login-siper.firebaseapp.com",
    projectId: "login-siper",
    storageBucket: "login-siper.appspot.com",
    messagingSenderId: "213502722529",
    appId: "1:213502722529:web:a169aad2bac62c3e9452a0",
    measurementId: "G-Z8LF91CRSC"
  }
  



};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
