import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RakbukuComponent } from './rakbuku.component';

describe('RakbukuComponent', () => {
  let component: RakbukuComponent;
  let fixture: ComponentFixture<RakbukuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RakbukuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RakbukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
