import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-rakbuku',
  templateUrl: './rakbuku.component.html',
  styleUrls: ['./rakbuku.component.scss']
})
export class RakbukuComponent implements OnInit {
  Rakbuku: any = [];
  timestamp: any;
  loading: boolean = false;

  constructor(public db: AngularFirestore,
    public dialog: MatDialog) { }

  ngOnInit(){
    this.getRakbuku();
  }

  dialogTambah(): void {
    const dialogRef = this.dialog.open(DialogTambahKategori, {
      width:'450px',
      data: {data: this.Rakbuku},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  dialogEdit(item: any): void {
    const dialogRef = this.dialog.open(DialogEditRak, {
      width:'450px',
      data: {data: item},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getRakbuku(){
    this.db.collection('rak-buku').valueChanges({idField: 'id'}).subscribe(res=>{
    this.Rakbuku = res;
    console.log(res);
    })
  }
  hapus(rowID: any) {
    var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
    if (r == true) {
      this.db.collection('rak-buku').doc(rowID).delete();
      alert('Pengguna baru berhasil dihapus');
    } else {
      return;
    }
  }
}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-tambah-kategori.html',
})
export class DialogTambahKategori {
  dataRak: any = {};
timestamp: any;
loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogTambahKategori>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){}

  addRak() {
    this.loading = true;
    var id_rak = this.dataRak.id;
    var dt = {
     id_rak: this.dataRak.id,
     nama_rak: this.dataRak.nama,
    }
    this.db.collection('rak-buku').doc(id_rak).set(dt).then(res => {
      this.loading = false;
      alert('Data Rak  berhasil di tambahkan');
      window.location.reload();
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-edit-rak.html',
})
export class DialogEditRak {
  data: any;

loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogEditRak>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    if (sourceData.data != null) {
      this.data = sourceData.data;
    }
  }
  
  editData(){
    this.loading = true;
    if (this.data != null) {
      this.db.collection('rak-buku').doc(this.data.id_rak).update(this.data).then(res => {
        alert('Data berhasil diperbarui.');
        this.dialogRef.close();
      })
    } else {
      alert('Gagal Memperbarui Data.');
      this.dialogRef.close();
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
