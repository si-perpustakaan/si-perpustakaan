import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-datasiswa',
  templateUrl: './datasiswa.component.html',
  styleUrls: ['./datasiswa.component.scss']
})
export class DatasiswaComponent implements OnInit {

  Datasiswa: any = [];
  timestamp: any;
loading: boolean = false;

  constructor(public db: AngularFirestore,
    public dialog: MatDialog) { }

  ngOnInit(){
    this.getDatasiswa();
  }
  dialogTambah(): void {
    const dialogRef = this.dialog.open(DialogTambahSiswa, {
      width:'450px',
      data: {data: this.Datasiswa},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  dialogEdit(data: any): void {
    const dialogRef = this.dialog.open(DialogEditSiswa, {
      width:'450px',
      data: {data: data},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getDatasiswa(){
    this.db.collection('data-siswa').valueChanges({idField: 'id'}).subscribe(res=>{
    this.Datasiswa = res;
    console.log(res);
    })
  }

  hapus(rowID: any) {
    var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
    if (r == true) {
      this.db.collection('data-siswa').doc(rowID).delete();
      alert('Data siswa berhasil dihapus');
    } else {
      return;
    }
  }

}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-tambah-siswa.html',
})
export class DialogTambahSiswa {
  datasiswa: any = {};
timestamp: any;
loading: boolean = false;
dataKelas: any;
kelas: any = {};
  constructor(
    public dialogRef: MatDialogRef<DialogTambahSiswa>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    this.getDataKelas();
  }
getDataKelas(){
  this.db.collection('data-kelas').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataKelas = res;
  })
}
  addSiswa() {
    this.loading = true;
    var id_siswa = this.datasiswa.id;
    var dt = {
     id_siswa: this.datasiswa.id,
     kelas: this.kelas,
     nama: this.datasiswa.nama,
     status: this.datasiswa.status,
    }
    this.db.collection('data-siswa').doc(id_siswa).set(dt).then(res => {
      this.loading = false;
      alert('Data Siswa  berhasil di tambahkan');
      window.location.reload();
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-edit-siswa.html',
})
export class DialogEditSiswa {
  dataSiswa: any = {};
  data: any;
timestamp: any;
loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogEditSiswa>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    if (sourceData.data != null) {
      this.data = sourceData.data;
    }

  }

  editData() {
    this.loading = true;
    if (this.data != null) {
      this.db.collection('data-siswa').doc(this.data.id_siswa).update(this.data).then(res => {
        alert('Data berhasil diperbarui.');
        this.dialogRef.close();
      })
    } else {
      alert('Gagal Memperbarui Data.');
      this.dialogRef.close();
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
