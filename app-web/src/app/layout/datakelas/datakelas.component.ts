import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-datakelas',
  templateUrl: './datakelas.component.html',
  styleUrls: ['./datakelas.component.scss']
})
export class DatakelasComponent implements OnInit {

  Datakelas: any = [];

  constructor(public db: AngularFirestore,
    public dialog: MatDialog) { }

  ngOnInit(){
    this.getDatakelas();
  }

  dialogTambah(): void {
    const dialogRef = this.dialog.open(DialogTambahKelas, {
      width:'450px',
      data: {data: this.Datakelas},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  dialogEdit(item:any): void {
    const dialogRef = this.dialog.open(DialogEditKelas, {
      width:'450px',
      data: {data: item},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getDatakelas(){
    this.db.collection('data-kelas').valueChanges({idField: 'id'}).subscribe(res=>{
    this.Datakelas = res;
    console.log(res);
    })
  }

  hapus(rowID: any) {
    var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
    if (r == true) {
      this.db.collection('data-kelas').doc(rowID).delete();
      alert('Data kelas berhasil dihapus');
    } else {
      return;
    }
  }

}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-tambah-kelas.html',
})
export class DialogTambahKelas {
  dataKelas: any = {};
timestamp: any;
loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogTambahKelas>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){}
  
  idKelas: any;
  addKelas() {
    this.loading = true;
    console.log(this.dataKelas)
    var doc = new Date().getTime() + '' + Math.floor(Math.random() * 10000);
    this.idKelas = doc;
    var dt = {
      Id_kelas: this.idKelas,
     kelas: this.dataKelas.kelas,
    }
    this.db.collection('data-kelas').doc(this.idKelas).set(dt).then(res => {
      this.loading = false;
      alert('Data kelas  berhasil di tambahkan');
      window.location.reload();
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}


@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-edit-kelas.html',
})
export class DialogEditKelas {
  data: any;

loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogEditKelas>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    if (sourceData.data != null) {
      this.data = sourceData.data;
    }
  }
  
  editData(){
    this.loading = true;
    if (this.data != null) {
      this.db.collection('data-kelas').doc(this.data.Id_siswa).update(this.data).then(res => {
        alert('Data berhasil diperbarui.');
        this.dialogRef.close();
      })
    } else {
      alert('Gagal Memperbarui Data.');
      this.dialogRef.close();
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
