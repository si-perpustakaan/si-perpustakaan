import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatakelasComponent } from './datakelas.component';

describe('DatakelasComponent', () => {
  let component: DatakelasComponent;
  let fixture: ComponentFixture<DatakelasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatakelasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatakelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
