import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';
import { LayoutRoutingModule } from './layout-routing.module';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';
import { AsidenavbarComponent } from './asidenavbar/asidenavbar.component';
import { FooternavbarComponent } from './footernavbar/footernavbar.component';
import { SettingsnavbarComponent } from './settingsnavbar/settingsnavbar.component';
import { TransaksiComponent, DialogTambahPinjaman } from './transaksi/transaksi.component';
import { PengembalianComponent, DialogTambahPengembalian } from './pengembalian/pengembalian.component';
import { DatabukuComponent, DialogTambahBuku, DialogDetailBuku, DialogEditBuku } from './databuku/databuku.component';
import { KategoribukuComponent, DialogTambah, DialogEditKategori } from './kategoribuku/kategoribuku.component';
import { RakbukuComponent, DialogTambahKategori , DialogEditRak} from './rakbuku/rakbuku.component';
import { DatasiswaComponent,DialogTambahSiswa, DialogEditSiswa } from './datasiswa/datasiswa.component';
import { DatakelasComponent,DialogTambahKelas, DialogEditKelas } from './datakelas/datakelas.component';


import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    MatDatepickerModule,
    RouterModule.forRoot([

    ]),
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatNativeDateModule
  ],
  declarations: [
    TopnavbarComponent, 
    AsidenavbarComponent,
     FooternavbarComponent,
     SettingsnavbarComponent,
     TransaksiComponent,
     PengembalianComponent,
     DatabukuComponent,
     KategoribukuComponent,
     RakbukuComponent,
     DatasiswaComponent,
     DatakelasComponent, 
     DialogTambahPinjaman,
    DialogTambahKategori,
    DialogTambahBuku,
    DialogDetailBuku,
    DialogEditBuku,
    DialogEditRak,
    DialogTambah,
    DialogEditKategori,
    DialogTambahSiswa,
    DialogEditSiswa,
    DialogTambahKelas,
    DialogEditKelas,
    DialogTambahPengembalian
  
    ],
    entryComponents: [
DialogTambahPinjaman,
DialogTambahKategori,
DialogTambahBuku,
DialogDetailBuku,
DialogEditBuku,
DialogEditRak,
DialogTambah,
DialogEditKategori,
DialogTambahSiswa,
DialogEditSiswa,
DialogTambahKelas,
DialogEditKelas,
DialogTambahPengembalian
    ],
     exports: [
      TopnavbarComponent, 
      AsidenavbarComponent,
       FooternavbarComponent,
       SettingsnavbarComponent,
     
     ]
})
export class LayoutModule { }
