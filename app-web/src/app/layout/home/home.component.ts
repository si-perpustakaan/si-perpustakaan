import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private db: AngularFirestore
  ) { }

  ngOnInit(): void {
    this.getDatasiswa();
    this.getDataBuku();
    this.getDataPengembalian();
    this.getDataTransaksi();
  }

  dataSiswa:any = [];
  getDatasiswa(){
    this.db.collection('data-siswa').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataSiswa = res;
    })
  }

  dataBuku:any = [];
  getDataBuku(){
    this.db.collection('data-buku').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataBuku = res;
    })
  }

  dataPengembalian:any = [];
  getDataPengembalian(){
    this.db.collection('pengembalian').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataPengembalian = res;
    })
  }

  dataTransaksi:any = [];
  getDataTransaksi(){
    this.db.collection('transaksi').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataTransaksi = res;
    })
  }

}
