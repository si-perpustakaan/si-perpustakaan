import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';



@Component({
  selector: 'app-pengembalian',
  templateUrl: './pengembalian.component.html',
  styleUrls: ['./pengembalian.component.scss']
})
export class PengembalianComponent implements OnInit {
  Datapengembalian: any = [];
  data: any = {};
timestamp: any;
loading: boolean = false;

  constructor(public db: AngularFirestore,
    public dialog: MatDialog) { }

  ngOnInit(){
    this.getDatapengembalian();
  }

  dialogTambah(): void {
    const dialogRef = this.dialog.open(DialogTambahPengembalian, {
      width:'450px',
      data: {data: this.Datapengembalian},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getDatapengembalian(){
    this.db.collection('pengembalian').valueChanges({idField: 'id'}).subscribe(res=>{
    this.Datapengembalian = res;
    console.log(res);
    })
  }

}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-tambah-pengembalian.html',
})
export class DialogTambahPengembalian {
  dataSiswa: any = {};
timestamp: any;
loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogTambahPengembalian>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){}
  idPinjam: any;

  addPengembalian() {
    this.loading = true;
    console.log(this.dataSiswa)
    var doc = new Date().getTime() + '' + Math.floor(Math.random() * 10000);
    this.idPinjam = doc;
    var dt = {
      Id_siswa: this.idPinjam,
      nama: this.dataSiswa.nama,
      isbn: this.dataSiswa.isbn,
      judul: this.dataSiswa.judul,
      tgl: this.dataSiswa.tgl,
    }
    this.db.collection('pengembalian').doc(this.idPinjam).set(dt).then(res => {
      this.loading = false;
      alert('Data pengembalian berhasil di tambahkan');
      window.location.reload();
    })
  }
  generateKode(){
    var p = this.dataSiswa.tgl == undefined ? '' : this.dataSiswa.tgl;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}