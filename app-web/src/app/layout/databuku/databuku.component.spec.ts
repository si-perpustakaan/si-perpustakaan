import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabukuComponent } from './databuku.component';

describe('DatabukuComponent', () => {
  let component: DatabukuComponent;
  let fixture: ComponentFixture<DatabukuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatabukuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatabukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
