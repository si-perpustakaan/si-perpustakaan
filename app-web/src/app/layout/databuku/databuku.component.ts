import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImageUploaderComponent } from '../../image-uploader/image-uploader.component';


@Component({
  selector: 'app-databuku',
  templateUrl: './databuku.component.html',
  styleUrls: ['./databuku.component.scss']
})
export class DatabukuComponent implements OnInit {
  Databuku: any = [];
  timestamp: any;
  loading: boolean = false;

  constructor(
    public db: AngularFirestore,
    public dialog: MatDialog
    ) { }

  ngOnInit(){
    this.getDatabuku();
  }

  uploadSampul(item:any) {
    console.log(item)
    const dialogRef = this.dialog.open(ImageUploaderComponent, {
      width: '500px',
      data: { rasio: 1, data: item }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }

  dialogTambah(): void {
    const dialogRef = this.dialog.open(DialogTambahBuku, {
      width:'450px',
      data: {data: this.Databuku},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  dialogDetail(data: any): void {
    const dialogRef = this.dialog.open(DialogDetailBuku, {
      width:'450px',
      data: {data: data},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  dialogEdit(data: any): void {
    const dialogRef = this.dialog.open(DialogEditBuku, {
      width:'450px',
      data: {data: data},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
getDatabuku(){
  this.db.collection('data-buku').valueChanges({idField: 'id'}).subscribe(res=>{
  this.Databuku = res;
  console.log(res);
  })
}




hapus(rowID: any) {
  var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
  if (r == true) {
    this.db.collection('data-buku').doc(rowID).delete();
    alert('Data buku berhasil dihapus');
  } else {
    return;
  }
}

}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-tambah-buku.html',
})
export class DialogTambahBuku {
  dataBuku: any = {};
dataKategori: any;
dataRak: any;
timestamp: any;
kategori: any;
rak: any;
loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogTambahBuku>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){ 
    this.getDatabuku();
    this.getRakbuku();
  }

  getDatabuku(){
    this.db.collection('kategori-buku').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataKategori = res;
    console.log(res);
    })
  }
  getRakbuku(){
    this.db.collection('rak-buku').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataRak = res;
    console.log(res);
    })
  }

  addBuku() {
    this.loading = true;
    var isbn = this.dataBuku.isbn;
    console.log(this.dataBuku)
    var dt = {
      isbn: this.dataBuku.isbn,
      kategori: this.kategori,
      penulis: this.dataBuku.penulis,
      penerbit: this.dataBuku.penerbit,
      keterangan: this.dataBuku.keterangan,
      rakbuku: this.rak,
      judul: this.dataBuku.judul,
      status: this.dataBuku.status,
      tahunterbit: this.dataBuku.tahunterbit,
      
    }
    this.db.collection('data-buku').doc(isbn).set(dt).then(res => {
      this.loading = false;
      alert('Data buku berhasil di tambahkan');
      window.location.reload();
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-edit-buku.html',
})
export class DialogEditBuku {
  dataSiswa: any = {};
  data: any;
timestamp: any;
loading: boolean = false;
dataKategori: any;
dataRak: any;
  constructor(
    public dialogRef: MatDialogRef<DialogEditBuku>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    if (sourceData.data != null) {
      this.data = sourceData.data;
    }
this.getDatabuku();
this.getRakbuku();
  }

  editData() {
    this.loading = true;
    if (this.data != null) {
      this.db.collection('data-buku').doc(this.data.isbn).update(this.data).then(res => {
        alert('Data berhasil diperbarui.');
        this.dialogRef.close();
      })
    } else {
      alert('Gagal Memperbarui Data.');
      this.dialogRef.close();
    }
  }

  getDatabuku(){
    this.db.collection('kategori-buku').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataKategori = res;
    console.log(res);
    })
  }
  getRakbuku(){
    this.db.collection('rak-buku').valueChanges({idField: 'id'}).subscribe(res=>{
    this.dataRak = res;
    console.log(res);
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-detail-buku.html',
})
export class DialogDetailBuku {
  dataSiswa: any = {};
  data: any;
timestamp: any;
loading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<DialogDetailBuku>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    if (sourceData.data != null) {
      this.data = sourceData.data;
    }

  }
 

  onNoClick(): void {
    this.dialogRef.close();
  }

}