import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-kategoribuku',
  templateUrl: './kategoribuku.component.html',
  styleUrls: ['./kategoribuku.component.scss']
})
export class KategoribukuComponent implements OnInit {
  Kategoribuku: any = [];

  constructor(public db: AngularFirestore,
    public dialog: MatDialog) { }

  ngOnInit(){
    this.getKategoribuku();
  }

  dialogTambah(): void {
    const dialogRef = this.dialog.open(DialogTambah, {
      width:'450px',
      data: {data: this.Kategoribuku},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  dialogEdit(item:any): void {
    const dialogRef = this.dialog.open(DialogEditKategori, {
      width:'450px',
      data: {data: item},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  getKategoribuku(){
    this.db.collection('kategori-buku').valueChanges({idField: 'id'}).subscribe(res=>{
    this.Kategoribuku = res;
    console.log(res);
    })
  }

  hapus(rowID: any) {
    var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
    if (r == true) {
      this.db.collection('kategori-buku').doc(rowID).delete();
      alert('Data kategori berhasil dihapus');
    } else {
      return;
    }
  }
}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-tambah.html',
})
export class DialogTambah {
  dataKategori: any = {};
timestamp: any;
loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogTambah>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){}
  
  addKategori() {
    this.loading = true;
    var id_kategori = this.dataKategori.id;
    var dt = {
     id_kategori: this.dataKategori.id,
     kategori: this.dataKategori.kategori,
    }
    this.db.collection('kategori-buku').doc(id_kategori).set(dt).then(res => {
      this.loading = false;
      alert('Data kategori  berhasil di tambahkan');
      window.location.reload();
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-edit-kategori.html',
})
export class DialogEditKategori {
  data: any;

loading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<DialogEditKategori>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    if (sourceData.data != null) {
      this.data = sourceData.data;
    }
  }
  
  editData(){
    this.loading = true;
    if (this.data != null) {
      this.db.collection('kategori-buku').doc(this.data.id_kategori).update(this.data).then(res => {
        alert('Data berhasil diperbarui.');
        this.dialogRef.close();
      })
    } else {
      alert('Gagal Memperbarui Data.');
      this.dialogRef.close();
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}


