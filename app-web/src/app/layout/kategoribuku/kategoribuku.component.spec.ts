import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KategoribukuComponent } from './kategoribuku.component';

describe('KategoribukuComponent', () => {
  let component: KategoribukuComponent;
  let fixture: ComponentFixture<KategoribukuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KategoribukuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KategoribukuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
