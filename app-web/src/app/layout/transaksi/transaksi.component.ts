import { Component, OnInit, Inject, Pipe, PipeTransform } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-transaksi',
  templateUrl: './transaksi.component.html',
  styleUrls: ['./transaksi.component.scss']
})
export class TransaksiComponent implements OnInit {
Datatransaksi: any = [];
data: any = {};
timestamp: any;
loading: boolean = false;
  constructor(
    public db:AngularFirestore,
    public dialog: MatDialog) {
    
  }


  ngOnInit(): void {
    this.getDatapeminjaman();
  }

  dialogTambah(): void {
    const dialogRef = this.dialog.open(DialogTambahPinjaman, {
      width:'450px',
      data: {data: this.Datatransaksi},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

getDatapeminjaman(){
  this.db.collection('transaksi').valueChanges({idField: 'id'}).subscribe(res=>{
 this.Datatransaksi = res;
  })
}


}

@Component({
  selector: 'dialog-container',
  templateUrl: 'dialog-tambah-pinjaman.html',
})
export class DialogTambahPinjaman {
  dataSiswa: any = {};
timestamp: any;
loading: boolean = false;
pipe = new DatePipe('en-US');
  constructor(
    public dialogRef: MatDialogRef<DialogTambahPinjaman>,
    public db: AngularFirestore,
    private router:Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ){
    
  }
  idPinjam: any;

  addPinjam() {
    this.loading = true;
    console.log(this.dataSiswa)
    var doc = new Date().getTime() + '' + Math.floor(Math.random() * 10000);
    this.idPinjam = doc;

    var dt = {
      Id_siswa: this.idPinjam,
      nama: this.dataSiswa.nama,
      isbn: this.dataSiswa.isbn,
      judul: this.dataSiswa.judul,
      status: this.dataSiswa.status,
      tanggal: this.dataSiswa.tanggal,
      tanggal_kembali: this.dataSiswa.tanggal_kembali
    }
    this.db.collection('transaksi').doc(this.idPinjam).set(dt).then(res => {
      this.loading = false;
      alert('Data peminjaman berhasil di tambahkan');
      window.location.reload();
    })
  }
 generateKode(){
  var p1 = this.dataSiswa.tanggal == undefined ? '' : this.dataSiswa.tanggal;
  var p2 = this.dataSiswa.tanggal_kembali == undefined ? '' : this.dataSiswa.tanggal_kembali;
 }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
