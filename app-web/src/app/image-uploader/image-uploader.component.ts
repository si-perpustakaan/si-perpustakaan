import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';
import { from } from 'rxjs';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {
  rasio:any;
  path:any;
  data:any;

  constructor(
    public dialogRef: MatDialogRef<ImageUploaderComponent>,
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    public storage:AngularFireStorage,
    @Inject(MAT_DIALOG_DATA) public sourceData: any
  ) {
    this.data = sourceData.data;
    console.log(this.data)
    this.rasio=sourceData.rasio;
    if(this.data.sampul != undefined) {
      this.parseImages();
    }
   }

  ngOnInit() {
  }

  imageChangedEvent: any = '';
  croppedImage: any = '';
  
  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

  loading:boolean = false; 
  unggahGambar() {
    this.loading=true;
    var doc = new Date().getDate() + new Date().getTime();
    var path = 'sampul/' + this.data.id + '/' + doc + '.png';
    this.storage.ref(path).putString(this.croppedImage, 'data_url').then(res => {       
      this.getUrl(path);       
    }); 
  }

  imagesBook:any = [];
  parseImages() {
    for(var i=0; i<this.data.sampul.length; i++) {
      this.imagesBook.push(this.data.images[i]);
    }
  }

  dataFoto:any;
  getUrl(path: any) {
    this.storage.ref(path).getDownloadURL().subscribe(res => {
      this.loading=false;
      this.dataFoto = {
        'url': res
      }
      if(this.dataFoto != undefined) {
        this.updateSampul();
      }
    });
  }

  updateSampul() {
    var dt = {
      sampul: this.dataFoto
    };
    this.db.collection('data-buku').doc(this.data.id).update(dt).then(res => {
      this.loading = false;
      alert('Gambar Sampul Berhasil Diperbarui');
      this.imageChangedEvent = '';
      this.croppedImage = '';
      this.onNoClick();
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
