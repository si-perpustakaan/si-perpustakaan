import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent} from '../layout/home/home.component';
import {TransaksiComponent} from '../layout/transaksi/transaksi.component';
import {PengembalianComponent} from '../layout/pengembalian/pengembalian.component';
import {DatabukuComponent} from '../layout/databuku/databuku.component';
import {KategoribukuComponent} from '../layout/kategoribuku/kategoribuku.component';
import {RakbukuComponent} from '../layout/rakbuku/rakbuku.component';
import {DatasiswaComponent} from '../layout/datasiswa/datasiswa.component';
import {DatakelasComponent} from '../layout/datakelas/datakelas.component';

const Myroutes: Routes = [
  {
    path: 'dashboard', component: DashboardComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'transaksi', component: TransaksiComponent },
      {path: 'pengembalian', component: PengembalianComponent},
      {path: 'databuku', component: DatabukuComponent},
      {path: 'kategoribuku', component: KategoribukuComponent},
      {path: 'rakbuku', component: RakbukuComponent},
      {path: 'datasiswa', component: DatasiswaComponent},
      {path: 'datakelas', component: DatakelasComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(Myroutes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
