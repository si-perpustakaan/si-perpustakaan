import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'daftar-pinjam',
    loadChildren: () => import('./pinjam/daftar-pinjam/daftar-pinjam.module').then( m => m.DaftarPinjamPageModule)
  },
  {
    path: 'riwayat',
    loadChildren: () => import('./pinjam/riwayat/riwayat.module').then( m => m.RiwayatPageModule)
  },
  {
    path: 'detail-buku/:isbn',
    loadChildren: () => import('./tab1/detail-buku/detail-buku.module').then( m => m.DetailBukuPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
