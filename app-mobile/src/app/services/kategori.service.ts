import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Kategori } from '../models/kategori';
import { map } from 'rxjs/operators';

@Injectable()

export class KategoriService 
{
  kategoriCol: AngularFirestoreCollection<Kategori>;
  kategoriDoc: AngularFirestoreDocument<Kategori>;
  kategoris: Observable<Kategori[]>;
  kategori: Observable<Kategori>;
  kategori$: any;

  constructor
  (
    private afs: AngularFirestore
  ) 
  
  { 
    this.kategoriCol = this.afs.collection('kategori-buku', ref => ref.orderBy('kategori'));

    this.kategoris = this.kategoriCol.snapshotChanges().pipe(
      map(action => {
        return action.map(
          a => 
          {
            const data = a.payload.doc.data() as Kategori;
              data.id_kategori = a.payload.doc.id;
              return data;
          }
        )
      })
    );
  } //end of constructor

  getKategoris()
  {
    return this.kategoris;
  } //end of get kategori list

  getKategori(id_kategori)
  { 
    this.kategoriDoc = this.afs.doc<Kategori>(`kategori-buku/${id_kategori}`);
    return this.kategori = this.kategoriDoc.valueChanges();
  } //end of get buku
}
