import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Buku } from '../models/buku';
import { map } from 'rxjs/operators';

@Injectable()
export class BukuService 
{
  bukuCol: AngularFirestoreCollection<Buku>;
  bukuDoc: AngularFirestoreDocument<Buku>;
  bukus: Observable<Buku[]>;
  buku: Observable<Buku>;
  buku$: any;
  // buku$: Observable<Buku>;


  constructor
  (
    private afs: AngularFirestore
  ) 
  {
    this.bukuCol = this.afs.collection('data-buku', ref => ref.orderBy('judul'));

    this.bukus = this.bukuCol.snapshotChanges().pipe(
      map(action => {
        return action.map(
          a => 
          {
            const data = a.payload.doc.data() as Buku;
              data.isbn = a.payload.doc.id;
              return data;
          }
        )
      })
    );
  } //end of constructor

  getBukus()
  {
    return this.bukus;
  } //end of get buku list

  getBuku(isbn)
  { 
    this.bukuDoc = this.afs.doc<Buku>(`data-buku/${isbn}`);
    return this.buku = this.bukuDoc.valueChanges();
  } //end of get buku
}
