import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, of } from 'rxjs';
import { User } from '../models/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { switchMap } from 'rxjs/operators';

@Injectable()

export class AuthService 
{
  user$: Observable<User>;
  user: User;


  constructor
  (
    private auth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private loadingCtrl: LoadingController,
    private toastr: ToastController
  ) 
  { 
    this.user$ = this.auth.authState.pipe(
      switchMap(user=>
      {
        if(user)
        {
          return this.afs.doc(`users/${user.uid}`).valueChanges();
        } else{
          return of(null);
        }
      }) 
    );


  } //end of constructor

  getUser()
  {
    return this.auth;
  } //end of get user list

  async login(email, pass)
  {  
    const loading = await this.loadingCtrl.create({
      message: 'Authenticating..',
      spinner: 'crescent',
      showBackdrop: true
    });

    loading.present();

    this.auth.auth.signInWithEmailAndPassword(email, pass).then((data)=> {
      if(!data.user.emailVerified)
      {
        loading.dismiss();
        this.toast('Please verified your email', 'danger');
        this.logout();
      } else{
        loading.dismiss();
        this.router.navigate(['m/tab1']);
      }
    })
    .catch(error => {
      loading.dismiss();
      this.toast(error.message, 'danger');
    })
  } // end of login

  logout()
  {
    this.auth.auth.signOut().then(()=> {
      this.router.navigate(['/login']);
    });
  }

  async toast(message, status)
  {
    const toast = await this.toastr.create({
      message:message,
      position: 'top',
      color: status,
      duration: 2000
    });

    toast.present();
  } // end of toast
}
