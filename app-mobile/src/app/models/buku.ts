export interface Buku 
{
    isbn: string;
    judul: string;
    keterangan: string;
    penerbit: string;
    penulis: string;
    rakbuku: string;
    status: string;
    kategori: string;
    sampul: string;
}
