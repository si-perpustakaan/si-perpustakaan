export interface User 
{
    userId: string;
    userName: string;
    userEmail: string;
    userPhone: string;
    userSex: string;
    userBirth: string;
    userFoto: string;
    createdAt: number;
}
