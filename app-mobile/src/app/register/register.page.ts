import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit 
{
  name: string;
  email: string;
  phone: string;
  password: string;
  sex: string;
  birth: string;
  confirmPassword: string;

  passwordMatch: boolean;

  constructor
  (
    private afs: AngularFirestore,
    private auth: AngularFireAuth,
    private loadingCtrl: LoadingController,
    private toastr: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  async register()
  {
    if(this.name && this.email && this.phone && this.password)
    {
      const loading = await this.loadingCtrl.create({
        message: 'loading..',
        spinner: 'crescent',
        showBackdrop: true
      });

      loading.present();

      this.auth.auth.createUserWithEmailAndPassword(this.email, this.password)
      .then((data) =>{
        this.afs.collection('users').doc(data.user.uid).set({
          'userId': data.user.uid,
          'userName': this.name,
          'userEmail': this.email,
          'userPhone': this.phone,
          'userSex': this.sex,
          'userBirth': this.birth,
          'createdAt': Date.now()
        });

        data.user.sendEmailVerification();

      })
      .then(() => {
        loading.dismiss();
        this.toast('Registration Success!', 'success');
        this.router.navigate(['/login']);
      })
      .catch((error) => {
        loading.dismiss();
        this.toast(error.message, 'danger');
      }) 
    } else{
      this.toast('Please fill the form!', 'danger');
    }
  } // end of register

  checkPassword()
  {
    if(this.password == this.confirmPassword)
    {
      this.passwordMatch = true;
    } else{
      this.passwordMatch = false; 
    }
  }

  async toast(message, status)
  {
    const toast = await this.toastr.create({
      message: message,
      position: 'top',
      color: status,
      duration: 2000
    });

    toast.present();
  } // end of toast

  login()
  {
    this.router.navigate(['/login']);
  }

}
