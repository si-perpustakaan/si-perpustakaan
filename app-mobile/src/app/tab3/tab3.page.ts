import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController, ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit
{
  users: any;

  constructor
  (
    public router: Router,
    private afuth: AngularFireAuth,
    private loadingCtrl: LoadingController,
    private toastr: ToastController,
    private auth: AuthService
  ) {}

  ngOnInit() 
  {
    this.auth.user$.subscribe(user => {
      this.users = user;
    })
  }

  logout()
  {
    this.afuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    })
  }

  forgot()
  {
    this.router.navigate(['/forgot-password']);
  } // end of forgot
}
