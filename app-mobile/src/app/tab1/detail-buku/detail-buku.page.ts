import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { BukuService } from '../../services/buku.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-detail-buku',
  templateUrl: './detail-buku.page.html',
  styleUrls: ['./detail-buku.page.scss'],
  providers: [BukuService]
})
export class DetailBukuPage implements OnInit
{
  isbn: string;
  judul: string;
  keterangan: string;
  penulis: string;
  penerbit: string;
  kategori:string;

  constructor
  (
    private route:ActivatedRoute,
    private bukuService: BukuService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private afs: AngularFirestore,
    private db: AngularFirestore
  ) { }

  ngOnInit()
  {
    // this.Id_siswa = this.route.snapshot.params['Id_siswa'];
    // this.bukuService.buku$.subscribe(buku => {
    //   this.buku = buku;
    // })

    this.isbn = this.route.snapshot.params['isbn'];
    this.loadBukuV2();
  }

  ionViewWillEnter()
  {
    this.loadBuku();
  }

  loadBukuV2()
  {
    this.db.collection('data-buku').doc(this.isbn).get().subscribe(res=>{
      console.log(res.data());
      this.getCat(res.data().kategori);
    })
  }

  getCat(id)
  {
    this.db.collection('kategori-buku').doc(id).get().subscribe(res=>{
      console.log(res.data());
    })
  }

  async loadBuku()
  {
    const loading = await this.loadingCtrl.create({
      message: 'Loading..',
      spinner: 'crescent',
      showBackdrop: true
    });

    loading.present();

    this.bukuService.getBuku(this.isbn).subscribe(buku => {
      this.judul = buku.judul;
      this.penulis = buku.penulis;
      this.penerbit = buku.penerbit;
      this.kategori = buku.kategori;
      this.keterangan = buku.keterangan;
     loading.dismiss();
    });
  }

}
