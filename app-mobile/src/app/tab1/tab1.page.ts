import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { KategoriComponent } from './kategori/kategori.component';
import { Router } from '@angular/router';
import { BukuService } from '../services/buku.service';
import { Buku } from '../models/buku';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  providers: [BukuService]
})
export class Tab1Page implements OnInit
{
  bukus: Buku[];
  sampleArr=[];
  resultArr=[];

  constructor
  (
    private bukuService: BukuService,
    private modalCtrl: ModalController, 
    private router: Router,
    private afs: AngularFirestore,
  ) 
  {}  

  ngOnInit()
  {
    this.bukuService.getBukus().subscribe(bukus => {
      this.bukus = bukus;
      console.log(this.bukus);
    });
  }


  async openModal(){
    const modal = await this.modalCtrl.create({
      component: KategoriComponent
    });
    modal.onDidDismiss().then(res=>{
      console.log(res);
    })
    await modal.present();
  }
  
  openDetail(isbn){
    this.router.navigate(['/detail-buku/',isbn]);
    console.log(this.router) ;
  }

  search(event){
    let searchKey:string=event.target.value;
    let firstLetter=searchKey.toUpperCase();

    if(searchKey.length==0){
      this.sampleArr=[];
      this.resultArr=[];
    }

    if(this.sampleArr.length==0){
      this.afs.collection('data-buku',ref => ref.where('judul','==',firstLetter)).snapshotChanges()
      .subscribe(data=>{
        data.forEach(childData => {
          this.sampleArr.push(childData.payload.doc.data())
        })
      })
    } else{
      this.resultArr=[];
      this.sampleArr.forEach(val=>{
        let name:string=val['judul'];
        if(name.toUpperCase().startsWith(searchKey.toUpperCase())){
          if(true){
            this.resultArr.push(val);
          }
        }
      })
    }
  }

}
