import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { KategoriService } from '../../services/kategori.service';
import { Kategori } from '../../models/kategori';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-kategori',
  templateUrl: './kategori.component.html',
  styleUrls: ['./kategori.component.scss'],
  providers: [KategoriService]
})
export class KategoriComponent implements OnInit
{
  kategoris: Kategori[];

  constructor
  (
    private modalCtrl: ModalController,
    private kategoriService: KategoriService,
    private router: Router,
    private afs: AngularFirestore,
  ) {}

  ngOnInit()
  {
    this.kategoriService.getKategoris().subscribe(kategoris => {
      this.kategoris = kategoris;
      console.log(this.kategoris);
    });
  }

  pilih(id)
  {
    this.modalCtrl.dismiss({data:id});
  }

  dismissModal() {
    this.modalCtrl.dismiss();
  } 

}
