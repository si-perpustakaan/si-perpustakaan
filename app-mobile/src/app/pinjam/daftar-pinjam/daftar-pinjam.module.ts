import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarPinjamPageRoutingModule } from './daftar-pinjam-routing.module';

import { DaftarPinjamPage } from './daftar-pinjam.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarPinjamPageRoutingModule
  ],
  declarations: [DaftarPinjamPage]
})
export class DaftarPinjamPageModule {}
