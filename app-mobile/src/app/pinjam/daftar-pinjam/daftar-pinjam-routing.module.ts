import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarPinjamPage } from './daftar-pinjam.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarPinjamPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarPinjamPageRoutingModule {}
